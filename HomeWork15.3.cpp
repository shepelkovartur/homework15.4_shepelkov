﻿#include <iostream>

const int N = 11;

//Функция четных чисел от 0 до N

void even(int N)
{
    if (N % 2 != 0)
    {
        N -= 1;
    }
    for (int i = 0; i < N + 2;)
    {
        std::cout << i << "\n";
        i += 2;
    }
}


//Функция нечетных чисел от 0 до N

void odd(int N)
{
    if ((N + 1) % 2 != 0)
    {
        N -= 1;
    }
    for (int i = 1; i < N + 2;)
    {
        std::cout << i << "\n";
        i += 2;
    }
}

int main()
{
    even(N);
    odd(N);
}